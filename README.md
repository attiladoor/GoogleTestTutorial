# GoogleTestTutorial

In the following brief tutorial, I will show you, how to install GoogleTest C++ framwork and how to start with a simple project.

### Install prerequirments

Please install the following packages:
```bash
sudo apt-get install cmake g++ git
```

### Build source without testing

First let's take a look what does our user program contain. 

In *Addition.h* and *Addition.cpp*, there is just only one class decalration which has only one static function which performs an integer addition. *Multiply.h* and *Multiply.cpp* is kind of the same but it performs multiplication. The demo program in *ExampleApp.cpp* is
pretty straight forward:
*ExampleApp.cpp:*
```c
#include "Addition.h"
#include "Multiply.h"

#include <stdio.h>

int main()
{
    int x = 4;
    int y = 5;

    int z1 = Addition::twoValues(x,y);
    printf("\nAddition Result: %d\n", z1);

    int z2 = Multiply::twoValues(x,y);
    printf("Multiply Result: %d\n", z2);

    return 0;
}

```

Let's build it and see the result:
```bash
cd src/
make
./exampleapp
> Addition Result: 9
> Multiply Result: 20
```

### Install GoogleTest

Clone the repository and install it from source
```bash
git clone https://github.com/google/googletest
cd googletest
cmake .
make
sudo make install
```

### TestFramework

You can read a detailed description of available function and utilities in GoogleTest framework here https://github.com/google/googletest/blob/master/googletest/docs/Primer.md but for now, let's check it briefly. 

There is a test cpp file for each class to separate test cases and make the program clear. Each testing class must be inherited from *::testing::Test* so they would contain the required facilities. Furthermore, class should contain a *SetUp()* and *TearDown* function.
The test functions are called by macros. We should pass the class name and member function for the macros. 

*Multiply_Test.cpp*:

```c
#include <limits.h>
#include "gtest/gtest.h"
#include "Multiply.h"

class MultiplyTest : public ::testing::Test {
 protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};

TEST_F(MultiplyTest,twoValues){
    const int x = 4;
    const int y = 5;
    Multiply multiply;
    EXPECT_EQ(20,multiply.twoValues(x,y));
    EXPECT_EQ(6,multiply.twoValues(2,3));
}
```

There are two type of macros, one as "ASSERT" and other one is as "EXPECT". The difference is, the "EXPECT" does not fail if the condition is not true. *Addition_Test.cpp* is the samme. 

*Main_TestAll.cpp*:
```c
#include <limits.h>
#include "gtest/gtest.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
```

As you can see, first we should initialize GoogeTest and then running *RUN_ALL_TESTS()* and let the framework do it's job. As you can see, this file also contains a *main()* function which means,
compiling the source file toget with *ExampleApp.cpp* would lead to a function redefiniton what we should avoid. 

*Makefile:*
```bash
CXX = g++
CXXFLAGS = -g -lgtest -lpthread
INCS = -I./ -I../../src  
OBJS = ../../src/Addition.o Addition_Test.o ../../src/Multiply.o Multiply_Test.o

testAll: $(OBJS)
	$(CXX) $(INCS) -o testAll  Main_TestAll.cpp $(OBJS) $(CXXFLAGS)

.cpp.o:
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(INCS)

clean:
	rm testAll *.o testAll.xml

```

As you can see, the object files of the original source files are not including the *ExampleApp.cpp*. Let's build the test file and run it.
```bash
cd test/src/
make
./testAll
[==========] Running 2 tests from 2 test cases.
[----------] Global test environment set-up.
[----------] 1 test from AdditionTest
[ RUN      ] AdditionTest.twoValues
[       OK ] AdditionTest.twoValues (0 ms)
[----------] 1 test from AdditionTest (0 ms total)

[----------] 1 test from MultiplyTest
[ RUN      ] MultiplyTest.twoValues
[       OK ] MultiplyTest.twoValues (0 ms)
[----------] 1 test from MultiplyTest (0 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 2 test cases ran. (0 ms total)
[  PASSED  ] 2 tests.
```

If you would like to export the result to xml, run:

```bash
testAll --gtest_output="xml:./testAll.xml"
```

The generated XML can be used to display result on Jenkins.